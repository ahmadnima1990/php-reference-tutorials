
<?php

define("ANIMAL",     "horse");

echo ANIMAL; // horse

echo '<br />';

define("ALL_ANIMALS",  array('cat', 'dog', 'fish'));

echo ALL_ANIMALS[1]; // dog 

define("MY_NAME", "AHMAD");

// Invalid constant names
define("3_ITEM",    "something");

echo '<br />';

// variables with reserved key  CONST
const MY_LAST_NAME = 'gholamnia';
echo MY_LAST_NAME;

echo '<br />';

const ALL_CARS = array('bens', 'bmw', 'maxima');
echo ALL_CARS[0];
?>