

<?php
// Control Structures

// match and switch

$statusCode = 200;

switch($statusCode) {
	case 200:
	case 300:
			$messageFromSwitch = 'OK';
			echo $messageFromSwitch;
			break;
			
	case 400:
			$messageFromSwitch = 'NOT FOUND';
			echo $messageFromSiwtch;
			break;
	
	case 500:
			$messageFromSwitch = 'server error';
			echo $messageFromSwitch;
			break;
			
	default: 
			$messageFromSwitch = 'unknown status code';
			echo $messageFromSwitch;
			break;
}



$messageFromMatch = match($statusCode) {
		200,300 => 'OK',
		400 => 'NOT FOUND',
		500 => 'server error',
		default => 'unknown status code',
}




?>

<?php  

echo '<br/>';
echo '<br/>';

// DECLARE TICKS

function onTick(){
	echo 'Tick<br />';
}
register_tick_function('onTick');

declare(ticks=1);

$i = 0;
$length = 10;

while($i < $length){
	echo $i++ . '<br />';
}

?>