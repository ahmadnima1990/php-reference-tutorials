
<?php
// Comparison Operators

  // Integers
echo 100 <=> 100; // 0
echo 112 <=> 113; // -1
echo 22 <=> 21; // 1
 
 echo '<br />';
// Floats
echo 10.5 <=> 10.5; // 0
echo 13.5 <=> 20.5; // -1
echo 20.5 <=> 30.5; // -1
echo '<br />';
  
// Strings
echo "ahmad" <=> "nima"; // -1
echo "ali" <=> "abas"; // 1
echo "a" <=> "aa"; // -1
echo "sssss" <=> "oo"; // 1
echo '<br />';

// Arrays
echo [] <=> []; // 0
echo ['a', 2, 3] <=> ['b', 2, 3]; // -1
echo [1, 2, 3] <=> []; // 1
echo [1, 2, 3] <=> [1, 2, 1]; // 1
echo [1, 2, 3] <=> [1, 2, 4]; // -1
echo '<br />';
echo '<br />';
// Objects
$a = (object) ["alireza" => "mohammad"]; 
$b = (object) ["ahmad" => "gholam"]; 
echo $a <=> $b; // 1   
//  ahmad  < gholam  ====> 1

// Incrementing/Decrementing Operators
echo '<br />';
echo '<br />';

$a = 12;
echo $a++; // 12
echo '<br />';
$b = 12;
echo ++$b; // 13
echo '<br />';
$a = 22;
echo $a--; // 22
echo '<br />';
$b = 22;
echo --$b; // 21


echo '<br />';
echo '<br />';

$s = 'A';
for($n=0;$n<16;$n++){
	echo ++$s; // BCDEFGHIJKLMNOPQ
}

echo '<br />';
echo '<br />';
// Logical Operators 

$aoa = false;
$bob = true;
$coc = false;
$dod = true;

echo $aoa && $bob; // false
echo $aoa || $dod; // true  or  1
echo !$aoa; // True or  1
echo $coc and $dod; // False
echo !$coc; // True or  1

echo '<br />';
echo '<br />';

$aaa = array("a" => "apple", "b" => "banana");
$bbb = array("a" => "pear", "b" => "strawberry", "c" => "cherry");

var_dump($aaa + $bbb); // Union of $a and $b
echo '<br />';
var_dump($bbb + $aaa); // Union of $a and $b

echo '<br />';
echo '<br />';

// Type Operators
class Family
{
}

class University
{
}
$ahmad = new Family;

var_dump($ahmad instanceof Family); // true
var_dump($ahmad instanceof University); // false



?>